﻿#include <vector>
#include <iostream>
#include <cmath>

using namespace std;

class Vector {
private:
    double x;
    double y;
    double z;

public:
    Vector(double x, double y, double z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    void Show() {
        cout << "x: " << x << ", y: " << y << ", z: " << z << endl;
    }

    double Len() {
        return sqrt(x * x + y * y + z * z);
    }
};

int main() {
    int x, y, z;
    cin >> x >> y >> z;
    Vector v(x, y, z);
    v.Show();
    cout << "Length: " << v.Len() << endl;

    return 0;
}
